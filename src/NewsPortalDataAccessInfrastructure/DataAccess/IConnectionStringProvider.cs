﻿namespace NewsPortal.Infrastructure.DataAccess
{
    public interface IConnectionStringProvider
    {
        /// <summary>
        /// Get connection string
        /// </summary>
        string ConnectionString { get; }
    }
}
