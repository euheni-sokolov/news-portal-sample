﻿using System.Configuration;

namespace NewsPortal.Infrastructure.DataAccess
{
    /// <summary>
    /// Connection string provider for *.config storages
    /// </summary>
    public abstract class BaseConnectionStringProvider : IConnectionStringProvider
    {
        private string _connectionString;

        protected abstract string ConnectionStringName { get; }

        public string ConnectionString
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_connectionString)){
                    _connectionString = GetConnectionString();
                }
                return _connectionString;
            }
        }

        /// <summary>
        /// Gets connection string using ConfigurationManager
        /// Should be overrided in case of another storage
        /// </summary>
        /// <returns></returns>
        protected virtual string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString;
        }
    }
}
