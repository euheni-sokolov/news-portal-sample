﻿namespace NewsPortal.Infrastructure.Enums
{
    public enum Role
    {
        Admin = 0x1,
        Author = 0x2,
        Approver = 0x4,
        Reader = 0x8,
    }
}
