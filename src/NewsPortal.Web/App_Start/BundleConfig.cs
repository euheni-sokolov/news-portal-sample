﻿using System.Web.Optimization;

namespace NewsPortal.Web
{
    /// <summary>
    /// Specifies the Bundle configuration.
    /// </summary>
    public class BundleConfig
    {
        /// <summary>
        /// Register bundles for project
        /// </summary>
        /// <param name="bundles">Collection of bundles</param>
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundle/base-styles")
               .Include("~/Content/bootstrap/bootstrap.css")
               .Include("~/Content/css/views/layout.css"));

            bundles.Add(new ScriptBundle("~/bundle/base-scripts")
               .Include("~/Scripts/jquery-2.1.4.js")
               .Include("~/Scripts/bootstrap.js"));
        }
    }
}