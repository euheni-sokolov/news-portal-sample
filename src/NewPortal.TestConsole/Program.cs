﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using NewsPortal.DataAccess.Context;
using NewsPortal.DataAccess.Context.Mongo;
using NewsPortal.DataAccess.Entities;
using NewsPortal.DataAccess.Infrastructure;
using NewsPortal.Infrastructure.Enums;

namespace NewPortal.TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionStringProvider = new MongoConnectionStringProvider();
            var dataContext = new MongoDataContext(connectionStringProvider);

            CleanUserDB(dataContext);

            TestDataContext(dataContext);
            CleanUserDB(dataContext);

            TestRepository(dataContext);
            CleanUserDB(dataContext);
        }

        static void TestDataContext(MongoDataContext dataContext)
        {
            Console.WriteLine("Test Data Context");
            var authors = new List<Author>
            {
                new Author
                {
                    Name = "author 1",
                    Email = "aut1@test.com",
                    ArticlesIds = new long[] {1,2,3 },
                },
                new Author
                {
                    Name = "author 2",
                    Email = "aut2@test.com",
                    ArticlesIds = new long[] {4},
                    Rating = (decimal?) 5.4,
                },
            };
            var readers = new List<Reader>
            {
                new Reader
                {
                    Name = "author 1",
                    Email = "aut1@test.com",
                    LastViewdArticles = new List<Article>
                    {
                        new Article
                        {
                            Id = new BsonObjectId(new ObjectId()),
                            Title = "article 1",
                            Uri = "some-uri"
                        }
                    },
                },
                new Reader
                {
                    Name = "author 2",
                    Email = "aut2@test.com",
                },
            };
            dataContext.GetCollection<Author>().InsertManyAsync(authors.ToArray());
            dataContext.GetCollection<Reader>().InsertManyAsync(readers.ToArray());

            var authorsFromDB = dataContext.GetCollection<Author>().Find(x => x.Role == (int)Role.Author).ToListAsync();

            foreach (var author in authorsFromDB.Result)
            {
                Console.WriteLine(author.Id);
                Console.WriteLine(author.Name);
                Console.WriteLine(author.Email);
                Console.WriteLine(author.Role);
                Console.WriteLine(author.Rating);
                Console.WriteLine(author.ArticlesIds != null
                    ? author.ArticlesIds.Aggregate(string.Empty, (current, articlesId) => current + $"{articlesId}, ")
                    : string.Empty);
                Console.WriteLine("===================================================================");
            }
            Console.ReadKey();

            var readersFromDB = dataContext.GetCollection<Reader>().Find(x => x.Role == (int)Role.Reader).ToListAsync();

            foreach (var reader in readersFromDB.Result)
            {
                Console.WriteLine(reader.Id);
                Console.WriteLine(reader.Name);
                Console.WriteLine(reader.Email);
                Console.WriteLine(reader.Role);
                Console.WriteLine(reader.LastViewdArticles != null
                    ? reader.LastViewdArticles.Aggregate(string.Empty, (current, article) => current + $"{article.Id} || {article.Title} || {article.Uri}")
                    : string.Empty);
                Console.WriteLine("===================================================================");
            }
            Console.ReadKey();
        }

        static void TestRepository(MongoDataContext dataContext)
        {
            Console.WriteLine("Test Repository");
            var authorRepository = new MongoRepository<Author>(dataContext);
            var readerRepository = new MongoRepository<Reader>(dataContext);

            var authors = new List<Author>
            {
                new Author
                {
                    Name = "author 1",
                    Email = "aut1@test.com",
                    ArticlesIds = new long[] {1,2,3 },
                },
                new Author
                {
                    Name = "author 2",
                    Email = "aut2@test.com",
                    ArticlesIds = new long[] {4},
                    Rating = (decimal?) 5.4,
                },
            };
            var readers = new List<Reader>
            {
                new Reader
                {
                    Name = "reader 1",
                    Email = "reader1@test.com",
                    LastViewdArticles = new List<Article>
                    {
                        new Article
                        {
                            Id = new BsonObjectId(new ObjectId()),
                            Title = "article 1",
                            Uri = "some-uri"
                        }
                    },
                },
                new Reader
                {
                    Name = "reader 2",
                    Email = "reader2@test.com",
                },
            };

            authorRepository.Create(authors);
            readerRepository.Create(readers);

            var oneMoreReader = new Reader
            {
                Name = "reader 3",
                Email = "reader3@test.com",
                LastViewdArticles = new List<Article>
                {
                    new Article
                    {
                        Id = new BsonObjectId(new ObjectId()),
                        Title = "article 2",
                        Uri = "some-uri2"
                    },
                    new Article
                    {
                        Id = new BsonObjectId(new ObjectId()),
                        Title = "article 3",
                        Uri = "some-uri3"
                    }
                },
            };
            readerRepository.Create(oneMoreReader);

            var authorsFromDB = authorRepository.Get().Where(x => x.Role == (int)Role.Author);

            foreach (var author in authorsFromDB)
            {
                Console.WriteLine(author.Id);
                Console.WriteLine(author.Name);
                Console.WriteLine(author.Email);
                Console.WriteLine(author.Role);
                Console.WriteLine(author.Rating);
                Console.WriteLine(author.ArticlesIds != null
                    ? author.ArticlesIds.Aggregate(string.Empty, (current, articlesId) => current + $"{articlesId}, ")
                    : string.Empty);
                Console.WriteLine("===================================================================");
            }
            Console.ReadKey();

            var readersFromDB = readerRepository.Get().Where(x => x.Role == (int)Role.Reader);

            foreach (var reader in readersFromDB)
            {
                Console.WriteLine(reader.Id);
                Console.WriteLine(reader.Name);
                Console.WriteLine(reader.Email);
                Console.WriteLine(reader.Role);
                Console.WriteLine(reader.LastViewdArticles != null
                    ? reader.LastViewdArticles.Aggregate(string.Empty, (current, article) => current + $"{article.Id} || {article.Title} || {article.Uri}")
                    : string.Empty);
                Console.WriteLine("===================================================================");
            }
            Console.ReadKey();

            Console.WriteLine("Change reader 3");
            oneMoreReader.Name += " changed";
            oneMoreReader.LastViewdArticles.RemoveAt(0);
            oneMoreReader.LastViewdArticles.Add(new Article
            {
                Id = new BsonObjectId(new ObjectId()),
                Title = "article 4",
                Uri = "some-uri4"
            });

            readerRepository.Update(oneMoreReader);
            var updatedReader = readerRepository.GetFirst(user => user.Id == oneMoreReader.Id);

            Console.WriteLine(updatedReader.Id);
            Console.WriteLine(updatedReader.Name);
            Console.WriteLine(updatedReader.Email);
            Console.WriteLine(updatedReader.Role);
            Console.WriteLine(updatedReader.LastViewdArticles != null
                ? updatedReader.LastViewdArticles.Aggregate(string.Empty, (current, article) => current + $"{article.Id} || {article.Title} || {article.Uri}")
                : string.Empty);
            Console.WriteLine("===================================================================");

            Console.ReadKey();


            Console.WriteLine("Delete reader 3, update other readers");

            readerRepository.Remove(user=>user.Email == updatedReader.Email);
            
            readerRepository.Update(new { Name = "mass updated name" },
                reader => reader.Email == "reader1@test.com" || reader.Email == "reader2@test.com");

            var readersFromDB1 = readerRepository.Get().Where(x => x.Role == (int)Role.Reader);

            foreach (var reader in readersFromDB1)
            {
                Console.WriteLine(reader.Id);
                Console.WriteLine(reader.Name);
                Console.WriteLine(reader.Email);
                Console.WriteLine(reader.Role);
                Console.WriteLine(reader.LastViewdArticles != null
                    ? reader.LastViewdArticles.Aggregate(string.Empty, (current, article) => current + $"{article.Id} || {article.Title} || {article.Uri}")
                    : string.Empty);
                Console.WriteLine("===================================================================");
            }
            Console.ReadKey();
        }

        static void CleanUserDB(MongoDataContext dataContext)
        {
            dataContext.GetCollection<Author>().DeleteManyAsync(user => user.Id != null);
        }
    }
}
