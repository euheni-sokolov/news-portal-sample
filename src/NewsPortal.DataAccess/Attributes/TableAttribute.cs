﻿namespace NewsPortal.DataAccess.Attributes
{
    /// <summary>
    /// Use to get name of the table and not hardcode it.
    /// If this attribute is not added table will be the same as class name 
    /// </summary>
    public class TableAttribute : System.Attribute
    {
        /// <summary>
        /// Name of table
        /// </summary>
        public string Name { get; set; }

        public TableAttribute(string name)
        {
            Name = name;
        }
    }
}
