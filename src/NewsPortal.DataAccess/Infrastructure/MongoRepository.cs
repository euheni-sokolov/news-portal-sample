﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using MongoDB.Driver;
using NewsPortal.DataAccess.Context;
using NewsPortal.DataAccess.Context.Mongo;
using NewsPortal.DataAccess.Entities;

namespace NewsPortal.DataAccess.Infrastructure
{
    public class MongoRepository<TEntity> : IRepository<TEntity> where TEntity : BaseMongoEntity
    {
        private MongoDataContext _context;

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="context"></param>
        public MongoRepository(MongoDataContext context)
        {
            _context = context;
        }

        public void Create(TEntity entity)
        {
            var result = _context.GetCollection<TEntity>().InsertOneAsync(entity);
            if (result.IsFaulted && result.Exception != null)
            {
                throw result.Exception;
            }
        }

        public void Create(IEnumerable<TEntity> entities)
        {
            var result = _context.GetCollection<TEntity>().InsertManyAsync(entities);
            if (result.IsFaulted && result.Exception != null)
            {
                throw result.Exception;
            }
        }

        public void Update(TEntity entity, Expression<Func<TEntity, bool>> filter = null)
        {
            filter = filter ?? (target => entity.Id == target.Id);
            var result = _context.GetCollection<TEntity>()
                            .ReplaceOneAsync(new ExpressionFilterDefinition<TEntity>(filter), entity);
            if (result.IsFaulted && result.Exception != null)
            {
                throw result.Exception;
            }
        }

        public void Save(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                var result = _context.GetCollection<TEntity>()
                                .ReplaceOneAsync(new ExpressionFilterDefinition<TEntity>(target => entity.Id == target.Id),
                                    entity, new UpdateOptions {IsUpsert = true} );

                if (result.IsFaulted && result.Exception != null)
                {
                    throw result.Exception;
                }
            }
        }

        public void Update(object fieldsToUpdate, Expression<Func<TEntity, bool>> filter)
        {
            UpdateDefinition<TEntity> update = null;
            foreach (var propertyInfo in fieldsToUpdate.GetType().GetProperties())
            {
                if (update == null)
                    update = Builders<TEntity>.Update.Set(propertyInfo.Name, propertyInfo.GetValue(fieldsToUpdate));
                else
                    update.Set(propertyInfo.Name, propertyInfo.GetValue(fieldsToUpdate));
            }
            //in general it will be better to use new ObjectUpdateDefinition<TEntity>(fieldsToUpdate) there
            //but for unknown reasons mongo throws exception. Thats why I'm using reflection there to build fields to update
            var result = _context.GetCollection<TEntity>()
                            .UpdateManyAsync(new ExpressionFilterDefinition<TEntity>(filter),
                                update, new UpdateOptions {IsUpsert = false});
            if (result.IsFaulted && result.Exception != null)
            {
                throw result.Exception;
            }
        }

        public IQueryable<TEntity> Get()
        {
            return _context.GetCollection<TEntity>().AsQueryable();
        }

        public TEntity GetFirst(Expression<Func<TEntity, bool>> filter = null)
        {
            var result =
                _context.GetCollection<TEntity>()
                    .Find(new ExpressionFilterDefinition<TEntity>(filter))
                    .FirstOrDefaultAsync();
            if (result.IsFaulted && result.Exception != null)
            {
                throw result.Exception;
            }
            return result.Result;
        }

        public void Remove(Expression<Func<TEntity, bool>> filter)
        {
            var result = _context.GetCollection<TEntity>().DeleteManyAsync(new ExpressionFilterDefinition<TEntity>(filter));
            if (result.IsFaulted && result.Exception != null)
            {
                throw result.Exception;
            }
        }

        public void Remove(TEntity entity)
        {
            var result = _context.GetCollection<TEntity>().DeleteOneAsync(new ExpressionFilterDefinition<TEntity>(target=>target.Id == entity.Id));
            if (result.IsFaulted && result.Exception != null)
            {
                throw result.Exception;
            }
        }
    }
}
