﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace NewsPortal.DataAccess.Infrastructure
{
    public interface IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Creates entity in database
        /// </summary>
        /// <param name="entity"></param>
        void Create(TEntity entity);

        /// <summary>
        /// Creates entities in database
        /// </summary>
        /// <param name="entities"></param>
        void Create(IEnumerable<TEntity> entities);

        /// <summary>
        /// Updates all fields of entity.
        /// </summary>
        /// <param name="entity">Entity</param>
        /// <param name="filter">Filter expression. If filter is not defined it filter by Id will be used by default</param>
        void Update(TEntity entity, Expression<Func<TEntity, bool>> filter = null);

        /// <summary>
        /// Updates all fields of entities.
        /// </summary>
        /// <param name="entities"></param>
        void Save(IEnumerable<TEntity> entities);

        /// <summary>
        /// Updates specified fields of entities that are satisfied filter.
        /// </summary>
        /// <param name="fieldsToupdate">Fields to update</param>
        /// <param name="filter">Filter expression.</param>
        void Update(object fieldsToupdate, Expression<Func<TEntity, bool>> filter);

        /// <summary>
        /// Get entities by filter
        /// </summary>
        /// <param name="filter">Filter expression</param>
        /// <returns></returns>
        IQueryable<TEntity> Get();

        /// <summary>
        /// Get first entity by filter
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        TEntity GetFirst(Expression<Func<TEntity, bool>> filter = null);

        /// <summary>
        /// Remove entities by filter
        /// </summary>
        /// <param name="filter"></param>
        void Remove(Expression<Func<TEntity, bool>> filter);

        /// <summary>
        /// Remove entity
        /// </summary>
        /// <param name="entity"></param>
        void Remove(TEntity entity);
    }
}
