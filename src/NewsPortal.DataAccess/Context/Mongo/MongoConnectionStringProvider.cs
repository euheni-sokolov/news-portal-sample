﻿using NewsPortal.Infrastructure.DataAccess;

namespace NewsPortal.DataAccess.Context.Mongo
{
    /// <summary>
    /// Connection string provider for NewsPortal mongo DB
    /// </summary>
    public class MongoConnectionStringProvider : BaseConnectionStringProvider
    {
        protected override string ConnectionStringName => "NewsPortalMongo";
    }
}
