﻿using System;
using MongoDB.Driver;
using NewsPortal.DataAccess.Attributes;
using NewsPortal.DataAccess.Entities;
using NewsPortal.Infrastructure.DataAccess;

namespace NewsPortal.DataAccess.Context.Mongo
{
    public class MongoDataContext : IMongoDataContext
    {
        private readonly IConnectionStringProvider _connectionStringProvider;

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="connectionStringProvider">ConnectionStringProvider to get connection string</param>
        public MongoDataContext(MongoConnectionStringProvider connectionStringProvider)
        {
            _connectionStringProvider = connectionStringProvider;
        }

        public IMongoCollection<T> GetCollection<T>() where T : BaseMongoEntity
        {
            var databaseName = MongoUrl.Create(_connectionStringProvider.ConnectionString).DatabaseName;
            var client = new MongoClient(_connectionStringProvider.ConnectionString);
            var database = client.GetDatabase(databaseName);

            var tableNameAttribute = Attribute.GetCustomAttribute(typeof(T), typeof(TableAttribute)) as TableAttribute;
            var tableName = tableNameAttribute == null ? typeof(T).Name : tableNameAttribute.Name;

            return database.GetCollection<T>(tableName);
        }
    }
}
