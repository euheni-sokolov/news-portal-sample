﻿using MongoDB.Driver;
using NewsPortal.DataAccess.Entities;

namespace NewsPortal.DataAccess.Context.Mongo
{
    public interface IMongoDataContext
    {
        /// <summary>
        /// Get Mongo collection to proceed mongo operations
        /// </summary>
        /// <typeparam name="T">Mongo entity type</typeparam>
        /// <returns>IMongoCollection that can be used to provide requests to Mongo DB</returns>
        IMongoCollection<T> GetCollection<T>() where T : BaseMongoEntity;
    }
}
