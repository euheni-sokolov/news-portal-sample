﻿using MongoDB.Bson.Serialization.Attributes;
using NewsPortal.DataAccess.Attributes;

namespace NewsPortal.DataAccess.Entities
{
    [Table("User")]
    public abstract class User : BaseMongoEntity
    {
        [BsonRequired]
        public string Name { get; set; }

        [BsonRequired]
        public string Email { get; set; }
        
        public abstract int Role { get; set; }
    }
}
