﻿using System.Collections.Generic;
using NewsPortal.DataAccess.Attributes;

namespace NewsPortal.DataAccess.Entities
{
    [Table("Users")]
    public class Reader : User
    {
        public override int Role
        {
            get { return (int)NewsPortal.Infrastructure.Enums.Role.Reader; }
            set {  }
        }

        public List<Article> LastViewdArticles { get; set; }
    }
}
