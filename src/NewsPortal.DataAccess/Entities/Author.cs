﻿using NewsPortal.DataAccess.Attributes;

namespace NewsPortal.DataAccess.Entities
{
    [Table("Users")]
    public class Author : User
    {
        public override int Role
        {
            get { return (int) NewsPortal.Infrastructure.Enums.Role.Author; }
            set {  }
        }

        public long[] ArticlesIds { get; set; }
        
        public decimal? Rating { get; set; }
    }
}
