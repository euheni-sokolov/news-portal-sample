﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace NewsPortal.DataAccess.Entities
{
    /// <summary>
    /// Base entity for Mongo data context
    /// Please use <typeparam name="NewsPortal.DataAccess.Attributes.TableAttribute"/> to set table name
    /// If this attribute is not added table will be the same as class name 
    /// </summary>
    public abstract class BaseMongoEntity
    {
        [BsonId]
        [BsonIgnoreIfDefault]
        public BsonObjectId Id { get; set; }
    }
}
