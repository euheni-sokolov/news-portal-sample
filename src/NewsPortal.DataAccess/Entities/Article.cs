﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NewsPortal.DataAccess.Attributes;

namespace NewsPortal.DataAccess.Entities
{
    [Table("Article")]
    public class Article : BaseMongoEntity
    {
        [BsonRequired]
        public string Title { get; set; }

        [BsonRequired]
        public string Uri { get; set; }
    }
}
